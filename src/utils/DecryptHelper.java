package utils;

import java.io.*;

public class DecryptHelper {
    public void decrypt() throws Exception {
        String S[] = new KeyExpander().fillS();
        System.out.println("Enter MESSAGE:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("A = ");
        String A = addZeros(Long.toBinaryString(Long.parseLong(reader.readLine(), 16)));
        System.out.print("B = ");
        String B = addZeros(Long.toBinaryString(Long.parseLong(reader.readLine(), 16)));
        int cur;
        for (int i = 12; i >= 1; --i) {
            B = addZeros(Long.toBinaryString((Long.parseLong(B, 2) - Long.parseLong(S[(2 * i) + 1], 2))));
            B = B.substring(B.length() - 32);
            cur = Integer.parseInt("" + Long.parseLong(A, 2) % 32);
            B = B.substring(B.length() - cur) + B.substring(0, B.length() - cur);
            B = ExclusiveOR(B, A);
            A = addZeros(Long.toBinaryString((Long.parseLong(A, 2) - Long.parseLong(S[2 * i], 2))));
            A = A.substring(A.length() - 32);
            cur = Integer.parseInt("" + Long.parseLong(B, 2) % 32);
            A = A.substring(A.length() - cur) + A.substring(0, A.length() - cur);
            A = ExclusiveOR(A, B);
            System.out.println("Iteration #" + i + " = " + (Long.toHexString(Long.parseLong(A, 2))) + (Long.toHexString(Long.parseLong(B, 2))));
        }
        A = addZeros(Long.toBinaryString((Long.parseLong(A, 2) - Long.parseLong(S[0], 2))));
        B = addZeros(Long.toBinaryString((Long.parseLong(B, 2) - Long.parseLong(S[1], 2))));
        String result = A + B;
        result = result.substring(result.length() - 64);
        System.out.println("Result output = " + (Long.toHexString(Long.parseLong(result.substring(0, 32), 2))) + (Long.toHexString(Long.parseLong(result.substring(32), 2))));
    }

    private String addZeros(String string) {
        return (getZeroString(32 - string.length()) + string);
    }

    private String ExclusiveOR(String first, String second) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < first.length(); ++i) {
            if (first.charAt(i) == second.charAt(i)) {
                stringBuilder.append("0");
            } else {
                stringBuilder.append("1");
            }

        }
        return stringBuilder.toString();
    }

    private String getZeroString(int length) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; ++i) {
            stringBuilder.append("0");
        }
        return stringBuilder.toString();
    }
}
