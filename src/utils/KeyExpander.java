package utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

class KeyExpander {
    private String S[] = new String[26];

    private String extend(String first, String second) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean check = false;
        for (int i = first.length() - 1; i >= 0; i--) {
            if ((first.charAt(i) == second.charAt(i) && !check) || (first.charAt(i) != second.charAt(i) && check)) {
                stringBuilder.insert(0, "0");
            } else {
                stringBuilder.insert(0,"1");
            }
            check = (first.charAt(i) == '1' && second.charAt(i) == '1')
                    || (first.charAt(i) == '1' && second.charAt(i) == '1' && check)
                    || (first.charAt(i) != second.charAt(i) && check);
        }
        return stringBuilder.toString();
    }

    String[] fillS() throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter KEY = ");
        String key = addZeros(reader.readLine());
        String L[] = new String[4];
        S[0] = Long.toBinaryString(Long.parseLong("B7E15163", 16));
        for (int i = 1; i < S.length; ++i) {
            S[i] = extend(S[i - 1], addZeros(Long.toBinaryString(Long.parseLong("9E3779B9", 16))));
        }
        for (int i = 0; i < L.length; ++i) {
            L[i] = (Long.toBinaryString(Long.parseLong(key.substring((3 - i) * 8, (((3 - i) + 1) * 8)), 16)));
        }
        System.out.println("Computed L values:");
        for (int i = 0; i < L.length; ++i) {
            System.out.println("L" + i + " = " + (Long.toHexString(Long.parseLong(L[i], 2))));
        }

        int i = 0, j = 0;
        String A = "", B = "", cur;
        for (int count = 0; count < 78; ++count) {
            cur = extend(addZeros(S[i]), extend(addZeros(A), addZeros(B)));
            A = S[i] = cur.substring(3) + cur.substring(0, 3);
            cur = extend(addZeros(L[j]), extend(addZeros(A), addZeros(B)));
            long le = (Long.parseLong(extend(addZeros(A), addZeros(B)), 2)) % 32;
            int len = Integer.parseInt("" + le);
            B = L[j] = cur.substring(len) + cur.substring(0, len);
            i = (i + 1) % 26;
            j = (j + 1) % 4;
        }
        return S;
    }

    private String addZeros(String string) {
        return (getZeroString(32 - string.length()) + string);
    }

    private String getZeroString(int length) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; ++i) {
            stringBuilder.append("0");
        }
        return stringBuilder.toString();
    }
}