package utils;

import java.io.*;

public class EncryptHelper {

    public void encrypt() throws Exception {
        String S[] = new KeyExpander().fillS();
        System.out.println("Enter MESSAGE:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("A = ");
        String A = addZeros(Long.toBinaryString(Long.parseLong(reader.readLine(), 16)));
        System.out.print("B = ");
        String B = addZeros(Long.toBinaryString(Long.parseLong(reader.readLine(), 16)));
        A = extend(A, addZeros(S[0]));
        B = extend(B, addZeros(S[1]));
        int cur;
        for (int i = 1; i <= 12; ++i) {
            cur = Integer.parseInt("" + Long.parseLong(B, 2) % 32);
            A = ExclusiveOR(A, B);
            A = A.substring(cur) + A.substring(0, cur);
            A = extend(A, addZeros(S[2 * i]));
            cur = Integer.parseInt("" + Long.parseLong(A, 2) % 32);
            B = ExclusiveOR(B, A);
            B = B.substring(cur) + B.substring(0, cur);
            B = extend(B, addZeros(S[(2 * i) + 1]));
            System.out.println("Iteration #" + i + " = " + (Long.toHexString(Long.parseLong(A, 2))) + (Long.toHexString(Long.parseLong(B, 2))));
        }
        String result = A + B;
        System.out.println("Result output = " + (Long.toHexString(Long.parseLong((result.substring(0, 32)), 2))) + (Long.toHexString(Long.parseLong((result.substring(32)), 2))));
    }

    private String addZeros(String string) {
        return (getZeroString(32 - string.length()) + string);
    }

    private String ExclusiveOR(String first, String second) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < first.length(); ++i) {
            if (first.charAt(i) == second.charAt(i)) {
                stringBuilder.append("0");
            } else {
                stringBuilder.append("1");
            }

        }
        return stringBuilder.toString();
    }

    private String getZeroString(int length) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; ++i) {
            stringBuilder.append("0");
        }
        return stringBuilder.toString();
    }

    private String extend(String first, String second) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean check = false;
        for (int i = first.length() - 1; i >= 0; i--) {
            if ((first.charAt(i) == second.charAt(i) && !check) || (first.charAt(i) != second.charAt(i) && check)) {
                stringBuilder.insert(0, "0");
            } else {
                stringBuilder.insert(0, "1");
            }
            check = (first.charAt(i) == '1' && second.charAt(i) == '1') ||
                    (first.charAt(i) == '1' && second.charAt(i) == '1' && check) ||
                    (first.charAt(i) != second.charAt(i) && check);
        }
        return stringBuilder.toString();
    }
}