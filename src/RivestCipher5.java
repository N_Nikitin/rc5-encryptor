import utils.DecryptHelper;
import utils.EncryptHelper;

public class RivestCipher5 {
    public static void main(String[] args) throws Exception {
        System.out.println("A and B - hex values. Cipher uses constants W = 32, R = 12.");
        System.out.println("Example input: \r\nKEY = 123456789123456789123456789\r\nA = 123\r\nB = 456\r\n");
        System.out.println("*-----------------------Encrypt----------------------*\r\n");
        EncryptHelper encryptHelper = new EncryptHelper();
        encryptHelper.encrypt();
        System.out.println("\r\nFor decryption, you can take output value.");
        System.out.println("*-----------------------Decrypt----------------------*\r\n");
        DecryptHelper decryptHelper = new DecryptHelper();
        decryptHelper.decrypt();
    }
}
